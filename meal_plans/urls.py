from django.urls import include, path

from meal_plans.views import (
    MealPlansListView,
    MealPlansCreateView,
    MealPlansDeleteView,
    MealPlansDetailView,
    MealPlansUpdateView,
)


urlpatterns = [
    path("", MealPlansListView.as_view(), name="meal_plans_list"),
    path("create/", MealPlansCreateView.as_view(), name="meal_plans_new"),
    path("<int:pk>/", MealPlansDetailView.as_view(), name="meal_plans_detail"),
    path("<int:pk>/edit/", MealPlansUpdateView.as_view(),
         name="meal_plans_edit"),
    path("<int:pk>/delete/", MealPlansDeleteView.as_view(),
         name="meal_plans_delete"),


]
