from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView

from meal_plans.models import MealPlans


class MealPlansListView(LoginRequiredMixin, ListView):
    model = MealPlans
    template_name = "meal_plans/list.html"
    context_object_name = "my_meal_plans"
    paginate_by = 10

    def get_queryset(self):
        return MealPlans.objects.filter(owner=self.request.user)


class MealPlansDetailView(LoginRequiredMixin, DetailView):
    model = MealPlans
    template_name = "meal_plans/detail.html"

    def get_queryset(self):
        return MealPlans.objects.filter(owner=self.request.user)


class MealPlansCreateView(LoginRequiredMixin, CreateView):
    model = MealPlans
    template_name = "meal_plans/new.html"
    fields = ["name", "recipes", "date"]
    success_url = reverse_lazy("meal_plans_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class MealPlansUpdateView(LoginRequiredMixin, UpdateView):
    model = MealPlans
    template_name = "meal_plans/edit.html"
    fields = ["name", "recipes", "date"]
    success_url = reverse_lazy("meal_plans")


class MealPlansDeleteView(LoginRequiredMixin, DeleteView):
    model = MealPlans
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy("meal_plans")

    def get_queryset(self):
        return MealPlans.objects.filter(owner=self.request.user)
