from django.db import models
from django.utils import timezone
from django.conf import settings

# Create your models here.
USER_MODEL = settings.AUTH_USER_MODEL


class MealPlans(models.Model):
    name = models.CharField(max_length=125)
    owner = models.ForeignKey(
        USER_MODEL,
        related_name="meal_plans",
        on_delete=models.CASCADE,
        null=True,
    )
    date = models.DateField(default=timezone.now)
    recipes = models.ManyToManyField("recipes.Recipe",
                                     related_name="meal_plans")

    # def __str__(self):
    #     return self.name
    def __str__(self):
        return self.name

    class Meta:
        # db_table = 'mealplans'
        # Add verbose name
        verbose_name = 'meal plan'
