from django.contrib import admin

# Register your models here.
from meal_plans.models import MealPlans


class MealPlansAdmin(admin.ModelAdmin):
    pass


admin.site.register(MealPlans, MealPlansAdmin)
